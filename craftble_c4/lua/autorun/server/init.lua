if(engine.ActiveGamemode() == "terrortown") then
    hook.Add("TTTPrepareRound", "ComponentC4Spawn", function()
        timer.Simple(5, function()
            local props = {}
            props = ents.FindByClass("weapon_*")
            local butyl = 0
            local oil = 0
            local geksogen = 0
            -- Spawn is capped to two max c4 assemblies
            for k, v in pairs(props) do
                if(geksogen < 3) then
                    local ent = ents.Create("e_geksogen")
                    if(ent:IsValid()) then
                        ent:SetPos(props[math.random(1, #props)]:GetPos())
                        ent:Spawn()
                        geksogen = geksogen + 1
                    end
                end
            end
            
            for k, v in pairs(props) do
                if(oil < 3) then
                    local ent = ents.Create("e_oil")
                        if(ent:IsValid()) then
                        ent:SetPos(props[math.random(1, #props)]:GetPos())
                        ent:Spawn()
                        oil = oil + 1
                    end
                end
            end
            
            for k, v in pairs(props) do
                if(butyl < 3) then
                    local ent = ents.Create("e_butyl")
                        if(ent:IsValid()) then
                        ent:SetPos(props[math.random(1, #props)]:GetPos())
                        ent:Spawn()
                        butyl = butyl + 1
                    end
                end
            end
        end)
    end)
end