AddCSLuaFile()

DEFINE_BASECLASS( "base_anim" )

ENT.Spawnable		            	 =  true
ENT.AdminSpawnable		             =  true 
ENT.PrintName		                 =  "C4"
ENT.Author			                 =  "eLeCtrOssSnake"
ENT.Contact			                 =  "electrosssnake@hotmail.com"
ENT.Category                         =  "ES Component C4"
ENT.IgnitionTime					 =  10
ENT.Exploded                         = false
ENT.Activated						 = false



function ENT:Initialize()
	self:SetModel("models/electrosssnake/c4.mdl")
	self.Exploded = false
	self:SetMoveType(MOVETYPE_VPHYSICS)
	self:SetSolid(SOLID_VPHYSICS)

	if (SERVER) then self:PhysicsInit(SOLID_VPHYSICS) end

	local phys = self:GetPhysicsObject()
	if (IsValid(phys)) then phys:Wake() end
	
end

function ENT:Explode()

	self:EmitSound( "ambient/explosions/explode_4.wav" )
	
	local detonate = ents.Create( "env_explosion" )
		--detonate:SetOwner(self.C4Owner)
		detonate:SetPos( self:GetPos() )
		detonate:SetKeyValue( "iMagnitude", "700" )
		detonate:Spawn()
		detonate:Activate()
		detonate:Fire( "Explode", "", 0 )
	
	local shake = ents.Create( "env_shake" )
		--shake:SetOwner( self.Owner )
		shake:SetPos( self:GetPos() )
		shake:SetKeyValue( "amplitude", "2000" )
		shake:SetKeyValue( "radius", "400" )
		shake:SetKeyValue( "duration", "2.5" )
		shake:SetKeyValue( "frequency", "255" )
		shake:SetKeyValue( "spawnflags", "4" )
		shake:Spawn()
		shake:Activate()
		shake:Fire( "StartShake", "", 0 )
	local c4pos = self:GetPos()

	self:Remove()
end
-- Real c4 doesn't explode when damaged
--[[
function ENT:OnTakeDamage(damage)
	if(self.Exploded == false) then
		self:Explode()
		self.Exploded = true
	end
end
--]]
function ENT:Use(activator, caller, useType, value)
	print(1)
	if(self.Activated == true) then return end
	print(2)
	if(engine.ActiveGamemode() == "terrortown" and GetRoundState() ~= ROUND_ACTIVE) then return end
	print(3)

	self:SetBodygroup(1, 1)
	timer.Simple(45, function() if(IsValid(self)) then self:Explode() end end)
	activator:ChatPrint("C4 has been planted. Timer set to 45 seconds.")
	self:EmitSound("C4.PlantSound")
	self.Activated = true
end