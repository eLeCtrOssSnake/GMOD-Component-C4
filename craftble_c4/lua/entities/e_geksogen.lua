AddCSLuaFile()

DEFINE_BASECLASS( "base_anim" )

ENT.Spawnable		            	 =  true
ENT.AdminSpawnable		             =  true 
ENT.PrintName		                 =  "Geksogen"
ENT.Author			                 =  "eLeCtrOssSnake"
ENT.Contact			                 =  "electrosssnake@hotmail.com"
ENT.Category                         =  "ES Component C4"
ENT.IgnitionTime					 =  10
ENT.Components						 = {}

function ENT:Initialize()
	self:SetModel("models/electrosssnake/geksogen.mdl")
	self.Components = {C_OIL = false, C_BUTYL = false}
	self:SetMoveType(MOVETYPE_VPHYSICS)
	self:SetSolid(SOLID_VPHYSICS)

	if (SERVER) then self:PhysicsInit(SOLID_VPHYSICS) end

	local phys = self:GetPhysicsObject()
	if (IsValid(phys)) then phys:Wake() end
	
end
-- Debug component status print
--[[
function ENT:Think()
	PrintTable(self.Components)
end
--]]

function ENT:StartTouch(entity)
	if(entity:GetClass() == "e_oil" and self.Components.C_OIL == false) then
		self.Components.C_OIL = true
		entity:Remove()
		if(self.Components.C_OIL and self.Components.C_BUTYL) then
			local c4 = ents.Create( "e_c4" )
			if ( !IsValid( c4 ) ) then return end
			c4:SetPos(self:GetPos())
			c4:Spawn()
			self:Remove()
		end
	elseif(entity:GetClass() == "e_butyl" and self.Components.C_BUTYL == false) then
		self.Components.C_BUTYL = true
		entity:Remove()
		if(self.Components.C_OIL and self.Components.C_BUTYL) then
			local c4 = ents.Create( "e_c4" )
			if ( !IsValid( c4 ) ) then return end
			c4:SetPos(self:GetPos())
			c4:Spawn()
			self:Remove()
		end
	end
end