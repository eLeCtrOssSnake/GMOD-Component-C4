AddCSLuaFile()

DEFINE_BASECLASS( "base_anim" )

ENT.Spawnable		            	 =  true
ENT.AdminSpawnable		             =  true 
ENT.PrintName		                 =  "Motor Oil"
ENT.Author			                 =  "eLeCtrOssSnake"
ENT.Contact			                 =  "electrosssnake@hotmail.com"
ENT.Category                         =  "ES Component C4"
ENT.IgnitionTime					 =  10

function ENT:Initialize()
	self:SetModel("models/electrosssnake/oil.mdl")

	self:SetPos(self:GetPos() + Vector(0, 0, 2))

	self:SetMoveType(MOVETYPE_VPHYSICS)
	self:SetSolid(SOLID_VPHYSICS)

	if (SERVER) then self:PhysicsInit(SOLID_VPHYSICS) end

	local phys = self:GetPhysicsObject()
	if (IsValid(phys)) then phys:Wake() end
	
end

function ENT:StartTouch(entity)
end